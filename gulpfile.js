'use strict'

const gulp            = require('gulp'),
      concat          = require('gulp-concat'),
      gutil           = require('gulp-util'),
      cleanDest       = require('gulp-clean-dest'),
      spritesmith     = require('gulp.spritesmith'),
      sass            = require('gulp-sass'),
      sourcemaps      = require('gulp-sourcemaps'),
      imageDataURI    = require('gulp-image-data-uri'),
      path            = require('path'),
      runSequence     = require('run-sequence'),
      modernizr       = require('modernizr'),
      fs              = require('fs'),
      svgstore        = require('gulp-svgstore'),
      svgmin          = require('gulp-svgmin'),
      ejs             = require('gulp-ejs'),
      gulpWebpack     = require('gulp-webpack'),
      webpack         = require('webpack'),
      connect         = require('gulp-connect'),
      autoprefixer    = require('gulp-autoprefixer'),
      eslint          = require('gulp-eslint'),
      htmllint        = require('gulp-htmllint'),
      ejsTmpl         = require('gulp-ejs-template'),
      gulpif          = require('gulp-if'),
      notifier        = require('node-notifier'),
      notify          = require('gulp-notify'),
      wait            = require('gulp-wait')

const DEBUG = ['prod', 'production'].indexOf(process.env.NODE_ENV) < 0

const IMAGE_COPY_PATHS = [
	'src/images/**/*',
	'!src/images/data-uri',
	'!src/images/data-uri/**/*',
	'!src/images/sprites',
	'!src/images/sprites/**/*',
	'!src/images/raw',
	'!src/images/raw/**/*',
	'!src/images/svgstore',
	'!src/images/svgstore/**/*'
]

var TASK_NOTIFICATION = false,
    LIVE_RELOAD = false

/**
 * Build UI texture atlas
 */
gulp.task('sprite:ui', () => {
	return gulp.src('src/images/sprites/ui/*.png')
		.pipe(spritesmith({
			imgName: 'dist/img/ui.png',
			imgPath: '../img/ui.png',
			cssName: 'src/styles/sprites/_ui.scss',
			imgOpts: {quality: 75},
			cssVarMap: sprite => {
				sprite.name = 'sprite-' + sprite.name;
				//console.log(sprite)
			}
		}))
		.pipe(gulp.dest('.'))
		.pipe(gulpif(LIVE_RELOAD, connect.reload()))
		.pipe(gulpif(TASK_NOTIFICATION, notify({ message: 'Sprite assembled.', onLast: true })))
})

/**
 * Build all texture atlases
 */
gulp.task('sprite', ['sprite:ui'])

/**
 * Build SASS -> CSS
 */
gulp.task('sass', () => {
	let main = gulp.src('src/styles/main.scss')
	if (DEBUG) {
		main = main.pipe(sourcemaps.init())
	}
	main = main.pipe(sass({
			outputStyle: 'compressed',
			sourcemap: true,
			includePaths: [
				__dirname + '/node_modules/foundation-sites/scss'
			],
			errLogToConsole: true
		}).on('error', sass.logError))
		.pipe(autoprefixer())
	if (DEBUG) {
		main = main.pipe(sourcemaps.write('./maps'))
	}
	// Remove piped files so that if there was an error
	// their old versions wont exist.
	return main.pipe(cleanDest('dist/css'))
		.pipe(gulp.dest('dist/css'))
		.pipe(gulpif(LIVE_RELOAD, connect.reload()))
		.pipe(gulpif(TASK_NOTIFICATION, notify({ message: 'SASS built.', onLast: true })))
})

/**
 * Creates SASS mixins with data-uri from raster images.
 */
gulp.task('data-uri:bitmaps', () => {
	const globs = ['*.jpeg', '*.jpg', '*.png', '*.gif']
	return gulp.src(globs.map(glob => path.join('src/images/data-uri', glob)))
		.pipe(imageDataURI({
			template: {
				file: './src/styles/data-uri/_bitmaps.scss.mustache',
			}
		}))
		.pipe(concat('_bitmaps.scss'))
		.pipe(gulp.dest('src/styles/data-uri'))
		.pipe(gulpif(LIVE_RELOAD, connect.reload()))
		.pipe(gulpif(TASK_NOTIFICATION, notify({ message: 'Data-uri SCSS files generated.', onLast: true })))
})

/**
 * Creates SASS mixins with data-uri from SVG images.
 */
gulp.task('data-uri:svg', () => {
	const globs = ['*.svg']
	return gulp.src(globs.map(glob => path.join('src/images/data-uri', glob)))
		.pipe(imageDataURI({
			template: {
				file: './src/styles/data-uri/_svg.scss.mustache',
			}
		}))
		.pipe(concat('_svg.scss'))
		.pipe(gulp.dest('src/styles/data-uri'))
		.pipe(gulpif(LIVE_RELOAD, connect.reload()))
		.pipe(gulpif(TASK_NOTIFICATION, notify({ message: 'Data-uri SCSS files generated.', onLast: true })))
})

/**
 * Base64-encode all vector and raster images to data-uri in SASS partial
 */
gulp.task('data-uri', ['data-uri:bitmaps', 'data-uri:svg'])

/**
 * Generates the modernizr.js containing preconfigured modernizer test code.
 */
gulp.task("modernizr", cb => {
	modernizr.build({
		'minify': false,
		'options': [
			'setClasses', 'addTest', 'mq'
		],
		"feature-detects": [
			'css/transitions', 'css/transforms', 'css/transforms3d', 'css/animations',
			'css/flexbox', 'css/backgroundsize', 'css/gradients'
		]
	}, function (result) {
		//const wrappedResult = result + '\nmodule.exports = window.Modernizr;'
		fs.writeFile(__dirname + "/src/scripts/modernizr.js", result, function(err) {
			if(err) {
				console.log(err)
			}
			cb()
		})
	})
})

/**
 * Combines svg files into one with <symbol> elements.
 * Read more about this here: http://css-tricks.com/svg-symbol-good-choice-icons/
 */
gulp.task('svgstore:ui', () => {
	return gulp.src('src/images/svgstore/ui/*.svg')
		.pipe(svgmin(file => {
			var prefix = path.basename(file.relative, path.extname(file.relative));
			return {
				plugins: [{
					cleanupIDs: {
						prefix: prefix + '-',
						minify: true
					}
				}],
				js2svg: {
					pretty: true
				}
			}
		}))
		.pipe(svgstore())
		.pipe(gulp.dest('dist/img'))
		.pipe(gulpif(LIVE_RELOAD, connect.reload()))
		.pipe(gulpif(TASK_NOTIFICATION, notify({ message: 'SVG store assembled.', onLast: true })))
})

/**
 * Combines svg files into one with <symbol> elements.
 */
gulp.task('svgstore', ['svgstore:ui'])

function ejsTask() {
	function entab(text, tabCount) {
		const tab = '\t'.repeat(tabCount)
		return text.split(/\r?\n/)
			.map(line => tab + line)
			.join('\n')
			.replace(/^\t+/, '')
	}
	let stub = JSON.parse(fs.readFileSync(__dirname + '/stub.json'))
	return gulp.src('src/markup-templates/*.ejs')
		.pipe(ejs(Object.assign({}, stub, {
			entab
		}), {}, {
			ext: '.html'
		}).on('error', gutil.log))
		.pipe(gulp.dest('dist'))
		.pipe(wait(1500))
		.pipe(gulpif(LIVE_RELOAD, connect.reload()))
		.pipe(gulpif(TASK_NOTIFICATION, notify({ message: 'EJS templates built.', onLast: true })))
}

/**
 * Builds EJS templates to HTML
 */
gulp.task('ejs', () => ejsTask())
gulp.task('__ejs', () => { ejsTask() })

gulp.task('htmllint', () => {
	return gulp.src('dist/*.html')
		.pipe(htmllint({}, (filepath, issues) => {
			if (issues.length > 0) {
				issues.forEach(function (issue) {
					gutil.log(
						gutil.colors.cyan('[gulp-htmllint] ') +
						gutil.colors.white(filepath + ' [' + issue.line + ',' + issue.column + ']: ') +
						gutil.colors.red('(' + issue.code + ') ' + issue.msg)
					);
				});
				if (!LIVE_RELOAD) {
					process.exitCode = 1;
				}
			}
		}))
})

/**
 * Copy images from src into dist folder.
 */
gulp.task('copy-images', () => {
	return gulp.src(IMAGE_COPY_PATHS)
		.pipe(gulp.dest('dist/img'))
		.pipe(gulpif(LIVE_RELOAD, connect.reload()))
		.pipe(gulpif(TASK_NOTIFICATION, notify({ message: 'Images copied.', onLast: true })))
})

/**
 * Copy web fonts.
 */
gulp.task('fonts', () => {
	return gulp.src('src/fonts/*/**')
		.pipe(gulp.dest('dist/fonts'))
		.pipe(gulpif(LIVE_RELOAD, connect.reload()))
		.pipe(gulpif(TASK_NOTIFICATION, notify({ message: 'Fonts copied.', onLast: true })))
})

gulp.task('ejs:rt', function () {
	return gulp.src(['src/runtime-templates/**/*'])
		.pipe(ejsTmpl({/*options*/}))
		.pipe(gulp.dest('src/scripts'))
		.pipe(gulpif(LIVE_RELOAD, connect.reload()))
		.pipe(gulpif(TASK_NOTIFICATION, notify({ message: 'Runtime EJS templates built.', onLast: true })))
})

gulp.task("js", cb => {
	return gulp.src('src/app.js')
		.pipe(gulpWebpack( require('./webpack.config.js')/*, webpack*/ ))
		.pipe(gulp.dest('dist/js'))
		.pipe(gulpif(LIVE_RELOAD, connect.reload()))
		.pipe(gulpif(TASK_NOTIFICATION, notify({ message: 'JS built.', onLast: true })))
})

gulp.task('watch', () => {
	LIVE_RELOAD = true
	TASK_NOTIFICATION = true
	connect.server({
		name: 'Dist App',
		root: 'dist',
		port: 8080,
		livereload: true
	});
	gulp.watch('src/styles/**/*.scss', ['sass'])
	gulp.watch('src/markup-templates/**/*', ['__ejs'])
	gulp.watch('src/runtime-templates/**/*', ['ejs:rt'])
	gulp.watch('src/scripts/**/*', ['js'])
	gulp.watch('src/images/sprites/**/*', ['sprite:ui'])
	gulp.watch('src/images/data-uri/*', ['data-uri'])
	gulp.watch('src/fonts/*/**', ['fonts'])
	gulp.watch(IMAGE_COPY_PATHS, ['copy-images'])
})

gulp.task('build', cb => {
	runSequence(
		['ejs:rt', 'modernizr', 'sprite', 'data-uri'],
		['sass', 'js'],
		['ejs', 'fonts', 'copy-images', 'svgstore'],
		cb)
})

gulp.task('default', ['watch'])