var DEFAULTS = {
	breakpoints: {},
	gutterX: 15,
	gutterY: 15
};

/**
 * Promo Grid widget
 */
var PromoGrid = function(settings) {
	$.extend(this, DEFAULTS, settings || {});
	if (!this.host) {
		throw new Error('PromoGrid constructor requires a `host` parameter');
	}
	this.$host = $(this.host);
	this.$items = this.$host.children();

	this.init();
};

$.extend(PromoGrid.prototype, {
	init: function() {
		this.relayout();
		$(window).resize($.proxy(this.relayout, this));
	},
	relayout: function() {
		var self = this;
		var areaWidth = this.$host.outerWidth();
		var breakpointsNames = this.getBreakpointsNames();
		var cols = this.getCols(breakpointsNames);
		var totalGutterWidth = this.gutterX * (cols - 1);
		// We've got the size (x|y) of 1x1 inner content area without any gutter.
		// For doubling the units we must also add 1 * gutter(X|Y) to total sum
		// to account the space between areas.
		var unitSize = (areaWidth - totalGutterWidth) / cols;
		var rows = this.getRows(breakpointsNames);
		var areaHeight = unitSize * rows + this.gutterY * (rows - 1);
		console.log(rows, breakpointsNames);
		
		this.$host.height(Math.ceil(areaHeight));
		this.$items.each(function() {
			var $item = $(this);
			var sx = $item.data('sx');
			var sy = $item.data('sy');
			var x = 1;
			var y = 1;
			var visible = true;
			breakpointsNames.forEach(function(bp) {
				var ix = parseInt($item.data(bp + '-x') || '');
				if (!isNaN(ix)) {
					x = ix;
				}
				var iy = parseInt($item.data(bp + '-y') || '');
				if (!isNaN(iy)) {
					y = iy;
				}
				var iVisible = $item.data(bp + '-visible');
				if (iVisible !== undefined) {
					visible = parseInt(iVisible) != 0;
				}
			});
			$item.css({
				display: visible ? 'block' : 'none',
				top: (y - 1) * (unitSize + self.gutterY),
				left: (x - 1) * (unitSize + self.gutterX),
				width: unitSize * sx + self.gutterX * (sx - 1),
				height: unitSize * sy + self.gutterY * (sy - 1)
			});
		});
	},
	/**
	 * Returns column count for the case when specified breakpoints are active.
	 */
	getCols: function(breakpointsNames) {
		var cols = 1;
		var self = this;
		breakpointsNames.forEach(function(bp) {
			var count = parseInt(self.$host.data(bp + '-cols') || '');
			if (!isNaN(count)) {
				cols = count;
			}
		});
		return cols;
	},
	/**
	 * Returns number of rows the content holds.
	 */
	getRows: function(breakpointsNames) {
		var maxY = 0;
		var self = this;
		this.$items.each(function() {
			var $item = $(this);
			var sy = $(this).data('sy');
			var y = 1;
			var visible = true;
			breakpointsNames.forEach(function(bp) {
				var iy = parseInt($item.data(bp + '-y') || '');
				if (!isNaN(iy)) {
					y = iy;
				}
				var iVisible = $item.data(bp + '-visible');
				if (iVisible !== undefined) {
					visible = parseInt(iVisible) != 0;
				}
			});
			if (visible) {
				maxY = Math.max(y + sy - 1, maxY);
			}
		});
		console.log(maxY);
		return maxY;
	},
	/**
	 * Returns current breakpoint value in px (or null in case of error).
	 */
	getBreakpointValue: function() {
		var bp = this.getBreakpointName();
		return (bp !== null) ? this.breakpoints[bp] : null;
	},
	/**
	 * Returns current breakpoint name (or null in case of error).
	 */
	getBreakpointName: function() {
		var breakpoints = this.getBreakpointsNames();
		if (breakpoints.length) {
			return breakpoints[breakpoints.length - 1];
		}
		return null;
	},
	/**
	 * Returns names of breakpoints currently active (in ascending order).
	 */
	getBreakpointsNames: function() {
		// Get and sort values
		var values = [];
		var valueToKey = {};
		for (var name in this.breakpoints) {
			if (this.breakpoints.hasOwnProperty(name)) {
				var value = this.breakpoints[name];
				values.push(value);
				valueToKey[value] = name;
			}
		}
		values = values.sort(function(a, b) { return a - b; });
		// Test breakpoints and get it names
		var names = [];
		for (var i = 0; i < values.length; i++) {
			if (Modernizr.mq('(min-width:' + values[i] + 'px)')) {
				names.push(valueToKey[values[i]]);
			} else {
				break;
			}
		}
		return names;
	}
});

module.exports = PromoGrid;