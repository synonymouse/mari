/**
 * $.tabs() plugin
 * 
 * It's not like $.tabs() in bootstrap, don't confuse with $.tabpanes()
 *
 * @author Alexander Korostin <coderlex@gmail.com>
 */
(function($) {
	'use strict'

	var Tabs = function($host, settings) {
		settings = settings || {};
		this.$host = $($host);
		this.disabled = false;
		this.bem = settings.bem || false;
	}

	$.extend(Tabs.prototype, {
		init: function() {
			this.$host.on('click', 'a', $.proxy(this.onAnchorClick, this));
		},
		disable: function(disable) {
			disable = disable === undefined ? true : disable;
			this.disabled = disable;
			var className = this.bem ? 'tabs--disabled' : 'disabled';
			this.$host.toggleClass('disabled', disable);
		},
		enable: function(enable) {
			return this.disable(!enable);
		},
		onAnchorClick: function(e) {
			e.preventDefault();
			if (this.disabled) {
				return;
			}
			var activeClass = this.bem ? 'tabs__item--active' : 'active';
			var $targetItem = $(e.currentTarget).parent();
			// Already here, do nothing
			if ($targetItem.hasClass(activeClass)) {
				return;
			}
			// Deactivate previous tab
			var $relatedItem = $targetItem.siblings().filter('.' + activeClass);

			this.$host.trigger($.Event('deactivate.tabs', {
				target: $relatedItem[0],
				relatedTarget: $targetItem[0]
			}));

			$relatedItem.removeClass(activeClass);

			this.$host.trigger($.Event('deactivated.tabs', {
				target: $relatedItem[0],
				relatedTarget: $targetItem[0]
			}));
			
			// Activate current tab

			this.$host.trigger($.Event('activate.tabs', {
				target: $targetItem[0],
				relatedTarget: $relatedItem[0]
			}));

			$targetItem.addClass(activeClass);

			this.$host.trigger($.Event('activated.tabs', {
				target: $targetItem[0],
				relatedTarget: $relatedItem[0]
			}));
		}
	});

	$.fn.tabs = function() {
		var args = Array.prototype.slice.call(arguments);
		return this.each(function() {
			var $host = $(this);
			var obj = $host.data('__tabs');
			if (!obj) {
				var settings = args.shift();
				var tabs = new Tabs($host, settings);
				tabs.init();
				$host.data('__tabs', tabs);
			} else {
				var method = args.shift();
				obj[method].apply(obj, args);
			}
		});
	};
})(jQuery);