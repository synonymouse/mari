/**
 * jQuery.fn.tabpane() plugin
 * @author Alexander Korostin <coderlex@gmail.com>
 */
(function($) {
	var DEFAULTS = {
		activePaneClass: 'active',
		panesSelector: '> *'
	};

	var TabPane = function(host, options) {
		this.options = $.extend({}, DEFAULTS, options);
		this.$host = $(host);
		this.$host.data('tabpane', this);
		this.$panes = this.$host.find(this.options.panesSelector);
		this.heightTransition = this.$host.data('height-transition') !== false;
		this.defaultDuration = this.$host.data('duration') || '200';
	};

	/**
	 * Returns whether the DOM node is initialized with TabPane widget.
	 */
	TabPane.isInit = function(el) {
		var widget = $(el).data('tabpane');
		return !!widget && widget.$host.is(el);
	};

	$.extend(TabPane.prototype, {
		show: function(paneEl) {
			var $pane = $(paneEl);
			var activePaneClass = this.options.activePaneClass;

			if ($pane.hasClass(activePaneClass)) {
				return;
			}

			var $prevPane = this.$host
				.find(this.options.panesSelector)
				.filter('.' + activePaneClass);

			var eventData = {
				target: $pane[0],
				relatedTarget: $prevPane[0]
			};
			var event = $.Event('hide.tabpane');
			$.extend(event, eventData);
			this.$host.trigger(event);

			var durationIn = parseInt($pane.data('duration') || this.defaultDuration);
			var durationOut = parseInt($prevPane.data('duration') || this.defaultDuration);
			var prevPaneHeight = $prevPane.height();
			this.$host.height(this.$host.height());

			$prevPane.fadeOut(durationOut, $.proxy(function() {
				$prevPane.removeClass(activePaneClass).css('display', '');
				this.$host.css('height', '');

				var event = $.Event('hidden.tabpane');
				$.extend(event, eventData);
				this.$host.trigger(event);

				$pane.addClass(activePaneClass);
				if (this.heightTransition) {
					// Save pane height
					var paneHeight = $pane.height();
					$pane.css({
						opacity: 0,
						// Set pane height at transition start
						height: prevPaneHeight + 'px'
					});
				} else {
					$pane.css('opacity', 0);
				}

				// This event fires on tab show, but before the new tab has been shown.
				var event = $.Event('show.tabpane');
				$.extend(event, eventData);
				this.$host.trigger(event);

				var props = { opacity: 1 };
				if (this.heightTransition) {
					props.height = paneHeight + 'px';
				}
				$pane.animate(props, durationIn, $.proxy(function() {
					$pane.css({ display: '', height: '', opacity: '' });

					var event = $.Event('shown.tabpane');
					$.extend(event, eventData);
					this.$host.trigger(event);
				}, this));
			}, this));
		}
	});

	$.fn.tabpane = function(options) {
		return this.each(function() {
			if (options === undefined || typeof options == 'object') {
				if (!TabPane.isInit(this)) {
					new TabPane(this, options);
				}
			} else if (options === 'show') {
				var $host = $(this).parent();
				if (!TabPane.isInit($host)) {
					throw new Error('$.tabpane() is not initialized on this node');
				}
				var widget = $host.data('tabpane');
				widget.show(this);
			} else {
				throw new Error('Incorrect $.tabpane() arguments');
			}
		});
	};
})(jQuery);