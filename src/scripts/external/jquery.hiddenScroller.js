/**
 * $.hiddenScroller() plugin
 *
 * @author Alexander Korostin <coderlex@gmail.com>
 */
(function($) {
	'use strict'

	// Adding some easing functions
	$.extend($.easing, {
		easeOutQuint: function (x, t, b, c, d) {
			return c*((t=t/d-1)*t*t*t*t + 1) + b;
		}
	});

	var DEFAULTS = {
		wrapContent: false,
		cursor: 'move',
		swipeInertia: 100,
		inertialExtent: 80
	};

	var HiddenScroller = function($host, options) {
		this.$host = $host;
		this.options = $.extend({}, DEFAULTS, options);
		this.startedPan = false;
	}

	$.extend(HiddenScroller.prototype, {
		init: function() {
			if (this.options.wrapContent) {
				this.$wrapper = $('<div/>');
				this.$host.children().remove().appendTo(this.$wrapper)
				this.$wrapper.appendTo($host)
			} else {
				this.$wrapper = this.$host.children();
			}

			this.$host.css({
				overflow: 'hidden'
			});
			this.$wrapper.css({
				position: 'relative',
				left: '0px'
			});

			this.relayout();

			$(window).resize($.proxy(this.relayout, this));

			var hammer = new Hammer(this.$host[0]);
			hammer.on('panstart', $.proxy(function(e) {
				if (this.options.swipeInertia) {
					// stop swipe inertia animation
					this.scrollToX(this.getScrollX(), {
						animate: false,
						jumpToEnd: false
					});
				}
				this.startScrollX = this.getScrollX();
				this.startedPan = true;
			}, this));
			hammer.on('panleft panright', $.proxy(function(e) {
				if (this.startedPan) {
					var scrollX = this.clampScrollX(this.startScrollX - e.deltaX);
					this.scrollToX(scrollX, {animate: false});
				}
			}, this));
			hammer.on('panend pancancel', $.proxy(function(e) {
				this.startedPan = false;

				// Add swipe inertia
				if (this.options.swipeInertia) {
					var inertialX = -e.velocityX * this.options.swipeInertia;
					var scrollX = this.clampScrollX(this.getScrollX() + inertialX);
					this.scrollToX(scrollX, {
						duration: 400,
						easing: 'easeOutQuint',
						// Correct position if it will be out of bound after
						// animation ends
						complete: $.proxy(this.relayout, this)
					});
				}

				if (this.options.inertialExtent) {
					this.relayout();
				}
			}, this));
		},
		clampScrollX: function(scrollX) {
			// prepare limits scrollX will clamp to
			var minScrollX = 0;
			var maxScrollX = this.getMaxScrollX();
			if (this.options.inertialExtent) {
				minScrollX -= this.options.inertialExtent;
				maxScrollX += this.options.inertialExtent;
			}
			// clamp to [minScrolX, maxScrollX]
			scrollX = Math.max(minScrollX, scrollX);
			scrollX = Math.min(maxScrollX, scrollX);
			return scrollX;
		},
		getMaxScrollX: function() {
			return Math.max(this.getOverflowSpaceX(), 0);
		},
		getScrollX: function() {
			return -parseInt(this.$wrapper.css('left'));
		},
		scrollToX: function(x, options) {
			options = $.extend({}, {
				animate: true,
				enqueue: false,
				jumpToEnd: true,
				duration: 100,
				easing: 'swing',
				complete: undefined
			}, options);

			this.$wrapper.stop(!options.enqueue, options.jumpToEnd);
			var css = { left: -x + 'px' };
			if (options.animate) {
				this.$wrapper.animate(css, options.duration, options.easing, options.complete);
			} else {
				this.$wrapper.css(css);
			}
		},
		getOverflowSpaceX: function() {
			return this.$wrapper.outerWidth(true) - this.$host.width();
		},
		relayout: function() {
			if (this.options.cursor) {
				if (this.getOverflowSpaceX() > 0) {
					this.$host.css({
						cursor: this.options.cursor
					});
				} else {
					this.$host.css({
						cursor: ''
					});
				}
			}
			// Correct scroller position if it got out of bounds
			var freeSpaceLeft = -this.getScrollX();
			var freeSpaceRight = this.getScrollX() - this.getOverflowSpaceX();
			if (freeSpaceLeft > 0) {
				this.scrollToX(0, {duration: 150});
			} else if (freeSpaceRight > 0) {
				this.scrollToX(this.getMaxScrollX(), {duration: 150});
			}
		}
	});

	$.fn.hiddenScroller = function(options) {
		return this.each(function() {
			var $host = $(this);
			if (!$host.data('__hidden-scroller')) {
				var scroller = new HiddenScroller($host, options);
				scroller.init();
				$host.data('__hidden-scroller', scroller);
			}
		});
	};
})(jQuery);