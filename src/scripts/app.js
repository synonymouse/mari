require('./modernizr');
require('./polyfills');

require('./external/jquery.tabs');
require('./external/jquery.hiddenScroller');
require('./external/jquery.tabpane');

var PromoGrid = require('./components/promo-grid');

/**
 * Application singleton
 */
var App = {
	config: {},
	init: function() {
		this.initTabs();
		this.initDropdowns();
		this.initSelectableDropdowns();
		this.initHamburger();
		this.initPrimaryNavSubmenus();
		this.initHeadSwiper();
		this.initSingleSwiper();
		this.initShopsSwiper();
		this.initPromoGrid();
		this.initPlan();
	},
	initTabs: function() {
		var $tabs = $('.tabs').tabs();
		$tabs.hiddenScroller();
		$('.tabs a').prop('draggable', false);
		$('.tab-panes').tabpane();
		$tabs.on('activated.tabs', function(e) {
			var $pane = $($(e.target).find('a').attr('href'));
			$pane.tabpane('show');
		});
	},
	initDropdowns: function() {
		$('[data-toggle=dropdown]').each(function() {
			var target = $(this).data('target') || $(this).attr('href');
			if (!target || target == '#') {
				var $target = $(this).closest('.dropdown');
			} else {
				var $target = $(target);
			}
			if (!$target) {
				throw new Error('Invalid dropdown target', this);
			}
			$(this).click(function(e) {
				e.preventDefault();
				$target.toggleClass('open');
				if ($target.hasClass('open')) {
					setTimeout(function() {
						$(document).one('click', function() {
							$target.removeClass('open');
						});
					}, 1);
				}
			});
		});
	},
	initSelectableDropdowns: function() {
		$('.selectable.dropdown').each(function() {
			var $text = $(this).children('a').children('span');
			$('.dropdown-menu a', this).click(function(e) {
				$text.text($(this).text());
				$(this).parent()
					.addClass('active')
					.siblings()
						.removeClass('active');
			});
		});
	},
	/**
	 * Initializes .plan widgets
	 */
	initPlan: function() {
		var floorZoneRegex = /^#(\d)-(\d)/;
		var floorRegex = /^#(\d)/;
		var floorCount = 5;

		var $plans = $('.plan').each(function() {
			var $plan = $(this);
			var $stage = $('.stage', this);
			var $dropdown = $('.dropdown', this);

			$(window).on('hashchange', updateFloorZoneByHash);
			updateFloorZoneByHash();

			function updateFloorZoneByHash() {
				var fz = getFloorZoneFromHash();
				updateFloor(fz.floor);
				updateZone(fz.zone);
			}

			function updateFloor(floor) {
				floor = floor || '';
				// Update .plan classes
				var classes = [];
				for (var i = 1; i <= floorCount; i++) {
					if (i != floor) {
						classes.push('floor-' + i);
					}
				}
				$plan.removeClass(classes.join(' '));
				if (floor) {
					$plan.addClass('floor-' + floor);
				}
				// Update dropdown
				$dropdown.find('.dropdown-menu a[href="#' + floor + '"]').click();
			}

			function updateZone(zone) {

			}
		});

		function getFloorZoneFromHash() {
			var ret = {};
			var match = location.hash.match(floorZoneRegex);
			if (match) {
				ret.floor = parseInt(match[1]);
				ret.zone = parseInt(match[2]);
			} else {
				match = location.hash.match(floorRegex);
				if (match) {
					ret.floor = parseInt(match[1]);
				}
			}
			return ret;
		}
	},
	/**
	 * Initializes hamburger (mobile) menu
	 */
	initHamburger: function() {
		var open = false;
		var $menu = $('.mobile-nav');
		var $hamburger = $('.hamburger').click(function() {
			open = !open;
			$hamburger.toggleClass('open', open);
			$menu.toggleClass('open', open);
			if (open) {
				setTimeout(function() {
					$(document).one('click', function() {
						open = false;
						$hamburger.removeClass('open');
						$menu.removeClass('open');
					});
				}, 10);
			}
		});
		$menu.click(function(e) {
			e.stopPropagation();
		});
	},
	/**
	 * Initializes .primary-nav submenus & dropdowns
	 */
	initPrimaryNavSubmenus: function() {
		$('.primary-nav').each(function() {
			var $root = $(this).children('ul');
			var $parentItems = $root.children('.has-children');
			var $parentLinks = $parentItems.children('a');

			$parentLinks.click(function(e) {
				e.preventDefault();
				var $item = $(this).parent();
				if ($item.hasClass('open')) {
					closeItem($item);
				} else {
					openItem($item);
				}
			});
		});
		function openItem($item) {
			$item.addClass('open');
			
			setTimeout(function() {
				$(document).one('click', function() {
					$item.removeClass('open');
				});
			}, 0);
		}
		function closeItem($item) {
			$item.removeClass('open');
		}
	},
	/**
	 * Initializes the swiper on index page
	 */
	initHeadSwiper: function() {
		$('.head-swiper').each(function() {
			new Swiper (this, {
				autoplay: 4000,
				direction: 'horizontal',
				loop: true,
				pagination: $('.swiper-pagination', this)[0],
				nextButton: $('.swiper-button-next', this)[0],
				prevButton: $('.swiper-button-prev', this)[0]
			});
		});
	},
	/**
	 * Initializes the swiper on index page
	 */
	initSingleSwiper: function() {
		$('.single-swiper').each(function() {
			new Swiper (this, {
				autoHeight: true,
				autoplay: 4000,
				direction: 'horizontal',
				loop: true,
				nextButton: $('.swiper-button-next', this)[0],
				prevButton: $('.swiper-button-prev', this)[0]
			});
		});
	},
	/**
	 * Initializes the swiper on index page
	 */
	initShopsSwiper: function() {
		$('.shops-swiper').each(function() {
			new Swiper($('.swiper-container', this)[0], {
				direction: 'horizontal',
				loop: true,
				nextButton: $('.swiper-button-next', this)[0],
				prevButton: $('.swiper-button-prev', this)[0],
				slidesPerView: 5,
				spaceBetween: 50,
				breakpoints: {
					1024: {
						slidesPerView: 4,
						spaceBetween: 40
					},
					768: {
						slidesPerView: 3,
						spaceBetween: 30
					},
					640: {
						slidesPerView: 2,
						spaceBetween: 20
					},
					400: {
						slidesPerView: 1,
						spaceBetween: 10
					}
				}
			});
		});
	},
	/**
	 * Initialize .promo-grid's metro-like plates
	 */
	initPromoGrid: function() {
		var settings = {
			breakpoints: {
				xxsmall: 0,
				xsmall: 440,
				small: 560,
				medium: 768,
				large: 992,
				xlarge: 1200,
				xxlarge: 1920
			}
		};
		$('.promo-grid').each(function() {
			var widget = new PromoGrid($.extend({}, settings, {
				host: this,
				gutterX: 30,
				gutterY: 30
			}));
		});
	},
	initMap: function() {
		var self = this;
		$('.contacts-map').each(function(index, mapEl) {
			var map = new google.maps.Map(mapEl, {
				zoom: Math.round(self.config.map.zoom),
				center: self.config.map.center,
				scrollwheel: false
			});
			var marker = new google.maps.Marker({
				position: self.config.map.center,
				map: map
			});
		});
	}
};

module.exports = App;
