var webpack = require('webpack')
var path = require('path')

module.exports = {
	target: 'web',
	devtool: 'source-map',
	context: __dirname + '/src/scripts',
	/*
	 * The entry point for the bundle
	 *
	 * See: http://webpack.github.io/docs/configuration.html#entry
	 */
	entry: {
		app: './app'
	},
	plugins: [
		//new webpack.OldWatchingPlugin(),
		/*
		 * Plugin: CommonsChunkPlugin
		 * Description: Shares common code between the pages.
		 * It identifies common modules and put them into a commons chunk.
		 *
		 * See: https://webpack.github.io/docs/list-of-plugins.html#commonschunkplugin
		 * See: https://github.com/webpack/docs/wiki/optimization#multi-page-app
		 */
		//new webpack.optimize.CommonsChunkPlugin('common', 'common.js'),
		new webpack.optimize.UglifyJsPlugin({
			compress: {warnings: false},
			output: {comments: false},
			minimize: true,
			sourceMap: true
		}),
	],
	output: {
		library: 'App',
		libraryTarget: 'var',
		publicPath: '/js/',
		path: __dirname + '/dist/js',
		filename: '[name].min.js',
		chunkFilename: 'chunk-[id].js'
	}
}